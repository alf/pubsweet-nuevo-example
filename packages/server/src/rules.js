module.exports = {
  projects: {
    get: (user) => true,
    post: (user, item, data) => user
  },
  project: {
    get: (user, item) => true,
    patch: (user, item, data) => user,
    delete: (user, item) => user._id.equals(item.owner)
  },
  users: {
    get: (user) => true, // user.admin
    post: (user, item, data) => user.admin
  },
  user: {
    get: (user, item) => user.admin || user.equals(item),
    patch: (user, item, data) => user.admin || user.equals(item),
    delete: (user, item) => user.admin
  }

  // TODO: just read/write?
  // TODO: match full path like Firebase?
  // TODO: read/write for properties
  // TODO: named mutations instead, for writing (note: doesn't solve reading)?
  // TODO: GraphQL?
}
