require('dotenv').config()

const { app } = require('@aeaton/pubsweet-server')

app({
  components: [
    require('@aeaton/pubsweet-server-authentication'),
    require('@aeaton/pubsweet-server-projects'),
    require('@aeaton/pubsweet-server-users')
  ],
  rules: require('./rules')
})
