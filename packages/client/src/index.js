import { app } from '@aeaton/pubsweet-client'

import App from './App'

import '@aeaton/pubsweet-theme-default'

app({
  App
})
