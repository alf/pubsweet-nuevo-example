import React from 'react'
import { Route, Redirect } from 'react-router-dom'

// TODO: let modules define these components, and their routes?
import { Login, Signup, PrivateRoute } from '@aeaton/pubsweet-client-authentication'
import { ProjectList, Project } from '@aeaton/pubsweet-client-projects'
import { UserList, User } from '@aeaton/pubsweet-client-users'

import AppBar from './AppBar'

const App = () => (
  <div>
    <Route path="/" exact render={() => <Redirect to="/projects"/>}/>

    <AppBar appName={process.env.REACT_APP_PUBSWEET_NAME}/>

    <div className="container">
      <PrivateRoute path="/users" exact component={UserList}/>
      <PrivateRoute path="/users/:user" exact component={User}/>

      <PrivateRoute path="/projects" exact component={ProjectList}/>
      <PrivateRoute path="/projects/:project" exact component={Project}/>

      <Route path="/login" exact component={Login}/>
      <Route path="/signup" exact component={Signup}/>
    </div>
  </div>
)

export default App
