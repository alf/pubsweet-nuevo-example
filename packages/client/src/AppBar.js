import React from 'react'
import { Nav, Navbar, NavbarBrand, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from '@aeaton/pubsweet-client-authentication'

const AppBar = ({ appName, user, logout }) => (
  <Navbar fluid staticTop style={{ minHeight: 0 }}>
    <Navbar.Header>
      <NavbarBrand>
        <Link to="/">{appName || 'Home'}</Link>
      </NavbarBrand>
    </Navbar.Header>

    {user.data && (
      <Nav pullRight>
        <LinkContainer to="/profile">
          <NavItem>logged in as {user.data.username}</NavItem>
        </LinkContainer>
        <NavItem onClick={logout}>logout</NavItem>
      </Nav>
    )}
  </Navbar>
)

export default withRouter(connect(
  state => ({
    user: state.user
  }),
  {
    logout: actions.logout
  }
)(AppBar))
